/*
File - connect.ts
    Access app.env file using dotenv
        npm install dotenv package to refer to environment variables defined in app.env file

*/

import {Client} from 'pg';
require('dotenv').config({path:'app.env'});

// takes in an object
export const client = new Client({
    user: process.env.DB_USER,
    password:process.env.DB_PASSWORD,
    database: process.env.DB_NAME, // You should never store passwords in code
    port: 5432,
    host: '34.66.164.120'
});
client.connect();