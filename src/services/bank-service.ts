
// your service interface should have all the methods that your RESTful web service that will find helpful
// 1. Methods that perform CRUD operation
// 2.  Business logic operations

import {Account, BankClient} from "../entities";

export default interface BankClientService{
    
    createClient(bankClientObj: BankClient): Promise<BankClient>;

    getAllClients(): Promise<BankClient[]>;

    getClientById(clientId: Number) : Promise <BankClient>;

    deleteClientById(clientId: Number): Promise<Boolean>;

    putClientById(clientObj: BankClient): Promise<BankClient>;
}