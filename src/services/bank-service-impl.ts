/* 
    author: Aishwarya 
    file: bank-service-impl
    purpose: {
        Methods that perform CRUD operation,
        Business logic operations
    }
*/

import { BankClient} from "../entities";
import { BankClientPostgres } from "../daos/bankClient-dao-postgres";
import BankClientService from "./bank-service";
// your service interface should have all the methods that your RESTful web service that will find helpful
//service methods for business logic


export class BankServiceImpl implements BankClientService{

    bankClientDAO:BankClientPostgres = new BankClientPostgres();

    createClient(bankClientObj: BankClient): Promise<BankClient>{
        return this.bankClientDAO.createClient(bankClientObj);
    }

    getAllClients(): Promise<BankClient[]> {
        return this.bankClientDAO.getAllClients();
    }


    getClientById(clientId: Number) : Promise<BankClient> {
        return this.bankClientDAO.getClientById(clientId);
    }

    deleteClientById(clientId: Number): Promise<Boolean>{
        return this.bankClientDAO.deleteClientById(clientId)
    }

    putClientById(clientObj: BankClient): Promise<BankClient>{
        return this.bankClientDAO.putClientById(clientObj);
    }


}