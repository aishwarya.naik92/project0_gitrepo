/*
    author: Aishwarya
    file: AccountServiceImpl interface declaration
    purpose: {
        Methods that perform CRUD operation,
        Business logic operations
    }
*/

import {Account} from "../entities";
import {AccountPostgres} from "../daos/account-dao-postgres";


export class AccountServiceImpl implements AccountServiceImpl{

    accountDAO:AccountPostgres = new AccountPostgres();

    createAccountForClient(accountObj: Account): Promise<Account>{
        return this.accountDAO.createAccountForClient(accountObj);
    }

    getAccountsForClient(clientId: number): Promise<Account[]> {
        return this.accountDAO.getAccountsForClient(clientId);
    }

    getAccountById(accountId: number): Promise<Account>{
        return this.accountDAO.getAccountById(accountId);
    }

    deleteAccountById(accountId: number): Promise<Boolean>{
        return this.accountDAO.deleteAccountById(accountId);
    }

    updateAccount(account: Account) : Promise<Account>{
        return this.accountDAO.updateAccount(account);
    }

    depositAmount(account: Account, depositAmount: number) : Promise<Boolean>{
        return this.accountDAO.depositAmount(account, depositAmount);
    }

    withdrawAmount(account: Account, withdrawAmount: number) : Promise<Boolean>{
        return this.accountDAO.withdrawAmount(account, withdrawAmount);
    }
}