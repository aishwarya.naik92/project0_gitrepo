/*
File: AccountService Interface declaration
*/
import {Account} from "../entities";

export default interface AccountService{

    //Create an account for client
    createAccountForClient(accountObj:Account ): Promise<Account>;

    //GET all Accounts for a Client by clientId
    getAccountsForClient(clientId: number) : Promise <Account[]>;

    //GET an account by accountId
    getAccountById(accountId: number): Promise<Account>;

    //DELETE account by accountId
    deleteAccountById(accountId: number): Promise<Boolean>;

    //UPDATE the account
    updateAccount(account: Account): Promise<Account>;

    //DEPOSIT money into ACccount
    depositAmount(account: Account, depositAmount: number): Promise<Boolean>;

    //WITHDRAW money from Account
    withdrawAmount(account: Account, withdrawAmount: number) : Promise<Boolean>;

}