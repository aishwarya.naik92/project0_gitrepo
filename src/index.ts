import express from 'express';
import { MissingResourceError } from './errors';

import BankClientService  from './services/bank-service';
import { BankServiceImpl } from './services/bank-service-impl';

import { BankClient, Account} from './entities';

import AccountService from './services/account-service';
import { AccountServiceImpl } from './services/account-service-impl';


const app = express();
app.use(express.json());

const bankClientService:BankClientService = new BankServiceImpl();
const accountService:AccountService = new AccountServiceImpl();

// get all clients
app.get("/clients", async(req,res)=> {
    const clients:BankClient[] = await bankClientService.getAllClients();
    console.log(clients);
    res.send(clients);
});


// create a client
app.post("/clients", async(req, res) => {
    let bankClientObj:BankClient = req.body;
    bankClientObj  = await bankClientService.createClient(bankClientObj);
    res.send(bankClientObj);
});


// get client by id
app.get("/clients/:id", async(req, res) => {
    try{
        const clientId = Number(req.params.id);
        const bankClient: BankClient = await bankClientService.getClientById(clientId);
        res.send(bankClient);
    }

    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});




app.delete("/clients/:id", async(req,res) => {
    try{
        const clientId = Number(req.params.id);
        //check if client exists first 
        const bankClient: BankClient = await bankClientService.getClientById(clientId);

        //if no misssing resource then we can go ahead and call the api 
        const result = await bankClientService.deleteClientById(bankClient.clientId);
        
        res.status(201); // upon success we return 201
        res.send(result); // return of the API
    }

    catch (error) {
        if(error instanceof MissingResourceError){
            
            res.status(404);
            res.send(error);
            
        }
    }
})

app.put("/clients/:id", async(req,res) => {
    try{
        const clientId = Number(req.params.id);
        let bankClientObj:BankClient = req.body;

        let retrievedClient:BankClient = await bankClientService.getClientById(clientId);

        retrievedClient.clientId = bankClientObj.clientId;
        retrievedClient.clientInfo.firstName = bankClientObj.clientInfo.firstName;
        retrievedClient.clientInfo.lastName = bankClientObj.clientInfo.lastName;
        retrievedClient.clientInfo.ssn = bankClientObj.clientInfo.ssn;

        //send the object that was sent by 
        const result:BankClient = await bankClientService.putClientById(retrievedClient);

        //after replacing check if created and can be found
        const updatedBankClient: BankClient = await bankClientService.getClientById(result.clientId);

        res.send(updatedBankClient);
    }

    catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})


/* For posting accounts of clients 
*/
app.post("/clients/:id/accounts", async(req, res) => {
    try{
        console.log("Working on creating a new account for a client......")
        const clientId = Number(req.params.id);
        const bankClient: BankClient = await bankClientService.getClientById(clientId);
        let accountObj: Account = req.body;
        accountObj.clientId = bankClient.clientId;
        console.log("retrievedclient ; "+ bankClient.clientId +" and accountObj.clientId: "+accountObj.clientId);
        accountObj = await accountService.createAccountForClient(accountObj);
        res.send(accountObj);
    }

    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});


app.get("/clients/:id/accounts", async(req, res) => {
    try{
        console.log("Working on getting all account for clients.....")
        const clientId = Number(req.params.id);
        const bankClient: BankClient = await bankClientService.getClientById(clientId);
        const allAccounts:Account[] = await accountService.getAccountsForClient(bankClient.clientId);
        res.send(allAccounts);
    }
    catch (error) {
        if(error instanceof MissingResourceError){
            
            res.status(404);
            res.send(error);
            
        }
    }
});


app.get("/accounts/:id", async(req,res) => {
    try {
        console.log("Working on getting account with id passed in......");
        const accountId = Number(req.params.id);
        const account:Account = await accountService.getAccountById(accountId);
        res.send(account);
    }

    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});


app.delete("/accounts/:id", async(req,res)=> {
    try{
        console.log("Working on deleting account with id passed in......");
        const accountId = Number(req.params.id);
        const result:Boolean = await accountService.deleteAccountById(accountId);
        res.send(result);

    }
    catch (error) {
        if(error instanceof MissingResourceError){
            
            res.status(404);
            res.send(error);
            
        }
    }
});


app.put("/accounts/:id", async(req,res) => {
    try{
        console.log(" Working on updating account with id passed in ......");

        const accountId = Number(req.params.id);
        const existingAccount:Account = await accountService.getAccountById(accountId);

        const account:Account = req.body;  //fetching the object from body 

        //written using guards, as we arent sure if we have all properties in body
        existingAccount.accType.account_type = account.accType.account_type || existingAccount.accType.account_type;
        existingAccount.amount = account.amount || existingAccount.amount;
        existingAccount.clientId = account.clientId || existingAccount.clientId;
        existingAccount.isActive = account.isActive || existingAccount.isActive;
        existingAccount.accountId = accountId;

        const updatedAccount:Account = await accountService.updateAccount(existingAccount);


        res.send(updatedAccount);
    }
    catch (error) {
        if(error instanceof MissingResourceError){
            
            res.status(404);
            res.send(error);
            
        }
    }
});

app.patch("/accounts/:id/deposit", async(req,res) => {
    try {
        console.log("Working on depositing amount into accountId given .....");

        const accountId = Number(req.params.id);
        const existingAccount:Account = await accountService.getAccountById(accountId);

        const amount:number = req.body.amount;
        

        const updatedResult:Boolean = await accountService.depositAmount(existingAccount, amount);
        res.send(updatedResult);
    }

    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.patch("/accounts/:id/withdraw", async(req, res) => {
    try {
        console.log("Working on withdrawing money from account id given .....");
        const accountId = Number(req.params.id);
        const withdrawAmount:number = req.body.amount;
        const existingAccount:Account = await accountService.getAccountById(accountId);
        if(existingAccount.amount < withdrawAmount)
        {
            res.status(422);
            res.send('Insufficient Funds. Try again with lower withdrawal amount');
        }
        else 
        {
            const updatedResult:Boolean = await accountService.withdrawAmount(existingAccount, withdrawAmount);
            res.send(updatedResult);
        }
    }
    catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})



app.listen(3000, ()=> {console.log("Application Started")});