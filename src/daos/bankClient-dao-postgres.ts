/*
File: bankClientDAO interface definition
*/

import {client} from '../connection';
import {MissingResourceError} from '../errors';
import {BankClient, ClientDetails} from '../entities';
import {BankClientDAO} from './bankClient-dao';


export class BankClientPostgres implements BankClientDAO{
    
    
    // Creates a new client
    // input parameter - bankClient objectType
    async createClient(bankClientObj: BankClient): Promise<BankClient>{
        const sql:string = "insert into BankClient(firstname, lastname, ssn) values ($1, $2, $3) returning clientid";
        const values = [bankClientObj.clientInfo.firstName, bankClientObj.clientInfo.lastName, bankClientObj.clientInfo.ssn];
        const result = await client.query(sql, values);
        console.log("After sql statement: " +result.rows[0].clientid);
        bankClientObj.clientId = result.rows[0].clientid;
        return bankClientObj;
    }

    // Gets all the clients
    // no input params
    async getAllClients(): Promise<BankClient[]>{
        const sql: string = 'select * from BankClient';
        const result = await client.query(sql);
        const clients:BankClient[] = [];
        for(const line of result.rows)
        {

            let detailsObj:ClientDetails =  {
                firstName : line.firstname,
                lastName: line.lastname,
                ssn: line.ssn
            };

            const bankClientObj:BankClient = new BankClient(
                line.clientid,
                detailsObj
            )
            clients.push(bankClientObj);
        }
        return clients;
    }

    // Gets the client stored by clientId if present
    // if no client present, then throws error
    async getClientById(clientId: Number): Promise<BankClient>{
        const sql:string = "select * from bankclient where clientid=$1";
        const values = [clientId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${clientId} does not exist`);
        }

        console.log("Result of getClientById: " +result.rows[0].firstname);
        const row = result.rows[0];

        let detailsObj:ClientDetails =  {
            firstName : row.firstname,
            lastName: row.lastname,
            ssn: row.ssn
        };

        const bankClient:BankClient = new BankClient(
            row.clientid,
            detailsObj
            
        )
        
        console.log("Result of getClientById: " +result.rows[0].firstname);
        return bankClient;
    }

    async deleteClientById(clientId: Number): Promise<Boolean> {
        const sql1:string = "delete from account where client_id=$1";
        const sql2:string = "delete from bankclient where clientid=$1";

        const values = [clientId];
        
        await client.query(sql1, values);
        const result = await client.query(sql2, values);

        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${clientId} does not exist`);
        }

        return true;

    }

    async putClientById(clientObj: BankClient): Promise<BankClient>{
        const sql: string = "update bankclient set firstname=$1, lastname=$2, ssn=$3 where clientid=$4";
        const values = [clientObj.clientInfo.firstName, clientObj.clientInfo.lastName, clientObj.clientInfo.ssn, clientObj.clientId];

        
        const result = await client.query(sql,values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`Could not update ${clientObj.clientId} `);
        }  

        return clientObj
    }


}