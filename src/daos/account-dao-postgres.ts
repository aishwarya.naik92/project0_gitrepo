/*
    File: AccountDAO interface implementation
    author: Aishwarya
*/

import { client } from "../connection";
import { Account, AccountType } from "../entities";
import { AccountDAO } from "../daos/account-dao";
import { MissingResourceError } from "../errors";

export class AccountPostgres implements AccountDAO{
    // Create an acccount for a client
    async createAccountForClient( testAccountObj: Account): Promise<Account> {
        
        const sql:string = "insert into Account(amount, account_type, isactive, client_id) values ($1, $2, $3, $4) returning accountid";
        const values = [testAccountObj.amount, testAccountObj.accType.account_type, testAccountObj.isActive, testAccountObj.clientId];
        const result = await client.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id blah does not exist`);
        }
        testAccountObj.accountId = result.rows[0].accountid;
        return testAccountObj;
    }


    // Fetch all accounts for client specified by a clientId
    async getAccountsForClient(clientId: number): Promise<Account[]> {
        const sql:string = "select * from account where client_id=$1";
        const values = [clientId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${clientId} does not exist`);
        }

        const accounts:Account[] = [];
        for(const line of result.rows)
        {
            const accountTypeObj:AccountType = {
                account_type: line.account_type
            }
            const accountObj:Account = new Account(
                line.accountid,
                line.ammount,
                accountTypeObj,
                line.isactive,
                line.clientid
            )
            accounts.push(accountObj);
        }
        return accounts;
    }

    //Get an account by id
    async getAccountById(accountId: number): Promise<Account>{
        const sql:string = "select * from account where accountid=$1";
        const values = [accountId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        const row = result.rows[0];

        const accountTypeObj:AccountType = {
            account_type: row.account_type
        }
        const account:Account = new Account(
            row.accountid,
            row.amount,
            accountTypeObj,
            row.isactive,
            row.client_id);

        return account ;
    }

    // Delete an account by id
    async deleteAccountById(accountId: number) : Promise<Boolean>{
        const sql:string = "delete from account where accountid=$1";
        const values = [accountId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        return true;
    }

    // Update an account
    async updateAccount(account: Account): Promise<Account>{
        const sql:string = "update account set amount=$1, account_type=$2, isactive=$3, client_id=$4 where accountid=$5";
        const values = [account.amount, account.accType.account_type, account.isActive, account.clientId, account.accountId];
        const result = await client.query(sql, values);

        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The account with id ${account.accountId} could not be updated`);
        }
        return account;
    }

    // Deposit sent amount to account
    async depositAmount(accountObj: Account, depositAmount: number) : Promise<Boolean>{
        const sql:string = "update account set amount = amount + $1 ";
        const values = [depositAmount];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The account with id ${accountObj.accountId} could not be found`);
        }
        return true;
    }

    // Withdraw money from account 
    async withdrawAmount(accountObj: Account, withdrawAmount: number): Promise<Boolean>{
        const sql:string = "update account set amount = amount - $1";
        const values = [withdrawAmount];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The account with id ${accountObj.accountId} could not be found`);
        }
        return true;
    }
}

