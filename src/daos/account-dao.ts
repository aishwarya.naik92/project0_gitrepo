/*
File: AccountDAO interface declaration
*/
import { Account } from "../entities";

export interface AccountDAO{

    // function - CREATE Account for CLIENT(BankClient)
    createAccountForClient(accountObj: Account): Promise<Account>;

    // function - GET accounts for client 
    getAccountsForClient(clientId: number): Promise<Account[]>;

    //function - GET account by ID
    getAccountById(accountId: number): Promise<Account>;

    //function - DELETE account by ID
    deleteAccountById(accountId: number): Promise<Boolean>;

    //function - UPDATE account 
    updateAccount(account: Account): Promise<Account>;

    //function - DEPOSIT amount into ACCOUNT
    depositAmount(account: Account, depositAmount: number): Promise<Boolean>;

    //function - WITHDRAW amount into ACCOUNT
    withdrawAmount(account: Account, withdrawAmount: number): Promise<Boolean>;
}