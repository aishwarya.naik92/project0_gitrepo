/*
File: bankClientDAO interface declaration
*/
import { BankClient, Account, AccountType } from "../entities";

export interface BankClientDAO{
    
    //function - CREATE a client 
    createClient(bankClient: BankClient) : Promise<BankClient>;

    // function - GET all clients
    getAllClients() : Promise<BankClient[]>;

    // function - GET client by id
    getClientById(clientId: Number) : Promise<BankClient>;

    // // function - GET accounts for client 
    // getAccountsForClient(clientId: number): PromiseLike<Account[]>;

    // function - delete client by id
    deleteClientById(clientId: Number) : Promise<Boolean>;

    putClientById(clientObj: BankClient): Promise<BankClient>;
}