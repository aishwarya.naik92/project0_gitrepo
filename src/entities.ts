/*
    File: entities/Resources
        - BankClient(Customer)
            Client has properties clientId, clientInfo of type CLientDetails(Type)
        - Account
            Account has properties accountId, amount, accType of type CheckingAccount(type) or SavingAccount(type)
            and 
    
*/ 

export type CheckingAccount = {
    account_type: "checking";
};

export type SavingsAccount = {
    account_type: "saving";
};

// Create a type which represents only one of the above types
// but you aren't sure which it is yet.
export type AccountType =
    | CheckingAccount
    | SavingsAccount;


export type ClientDetails = {
    firstName: string;
    lastName: string,
    ssn: number;
}

//Create an BankClient
export class BankClient{
    constructor(
        public clientId: number,
        public clientInfo: ClientDetails)
        {

        }
}


//Creating an account entity
export class Account {
    constructor(
        public accountId: number,
        public amount: number,
        public accType: AccountType ,
        public isActive: boolean, 
        public clientId: number)
    {

    }

}
