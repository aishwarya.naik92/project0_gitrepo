import { BankClient, Account, AccountType, ClientDetails } from '../src/entities';
import { BankClientDAO } from '../src/daos/bankClient-dao';
import { BankClientPostgres } from '../src/daos/bankClient-dao-postgres';
import { client } from '../src/connection';


let nameObj:ClientDetails =  {
    firstName : 'Ash',
    lastName: 'Naik',
    ssn: 2222
};

let testAccountType: AccountType = {
    account_type: "checking"
}

let testIsActive = true;
let testAmount = 10000;

const testBankClient: BankClient = new BankClient(0, nameObj);



//Data access object to use
export const bankClientDAO: BankClientDAO = new BankClientPostgres();


test("-----TC 1 : CREATE/POST a Client-----", async()=>{
    console.log("------Attempting to CREATE/POST all clients by ID-----");
    const result:BankClient = await bankClientDAO.createClient(testBankClient);
    console.log("------Done with READ/GET all clients by ID-----");
    expect(result.clientId).not.toBe(0); // an entity should have a non-zero value
})

test("-----TC 2 : READ/GET all Clients-----", async()=>{
    console.log("------Attempting to READ/GET all clients by ID-----");
    let client1DetailsObject:ClientDetails = {
        firstName: 'Ash1',
        lastName: 'Naik1',
        ssn: 23454
    } 
    const client1:BankClient = new BankClient(0, client1DetailsObject);

    let client2DetailsObject: ClientDetails = {
        firstName: 'Ash2',
        lastName: 'Naik2',
        ssn: 92384
    }

    const client2:BankClient = new BankClient(0, client2DetailsObject);

    await bankClientDAO.createClient(client1);
    await bankClientDAO.createClient(client2);

    const clients:BankClient[] = await bankClientDAO.getAllClients();
    console.log("------Done with READ/GET all clients by ID-----");
    expect(clients.length).toBeGreaterThanOrEqual(2);
});


test("-----TC 3: READ/GET a client by ID-----", async()=>{
    console.log("------Attempting to READ/GET a client by ID------");
    let clientDetailsObject:ClientDetails = {
        firstName: 'Ash3',
        lastName: 'Naik3',
        ssn: 34567
    } 
    let client:BankClient = new BankClient(0, clientDetailsObject);
    console.log("Printing cliendID: "+client.clientId);
    const createdClient:BankClient = await bankClientDAO.createClient(client);


    const retreievedClient = await bankClientDAO.getClientById(createdClient.clientId);
    console.log("------Done with READ/GET a client by ID------");
    expect(retreievedClient.clientInfo.ssn).toBe(clientDetailsObject.ssn);
});

// DELETE client by ID
test("-----TC 4: DELETE client by ID-----", async()=> {
    console.log("------Attempting to Delete a client by ID------");
    let clientDetailsObject:ClientDetails = {
        firstName: "Anu",
        lastName: "Shetty",
        ssn: 40943
    }

    let client:BankClient = new BankClient(0, clientDetailsObject);
    const createdClient:BankClient = await bankClientDAO.createClient(client);

    const retreievedClient = await bankClientDAO.getClientById(createdClient.clientId);
    const deleted:Boolean = await bankClientDAO.deleteClientById(retreievedClient.clientId);
    console.log("------Done deleting a client by ID------");
    expect(deleted).toBeTruthy();
});

// Update client by ID
test("-----TC 5: UPDATE/PUT client by ID-----", async()=>{
    console.log("------Attempting to update a client------");
    // using an already created client, to check if present
    let retrievedClient = await bankClientDAO.getClientById(testBankClient.clientId);

    retrievedClient.clientInfo.firstName = "John";
    retrievedClient.clientId = testBankClient.clientId;
    retrievedClient.clientInfo.lastName = "Doe";
    retrievedClient.clientInfo.ssn = 90482;

    const updatedClient:BankClient = await bankClientDAO.putClientById(retrievedClient);
    const verifiedClientObj = await bankClientDAO.getClientById(updatedClient.clientId);
    console.log("------Done Updating a client------");
    expect(verifiedClientObj.clientId).toEqual(testBankClient.clientId);
});

afterAll(async()=>{
    client.end();// should close our connection once all tests is over
});