import { client } from "../src/connection";
import {BankClientPostgres} from '../src/daos/bankClient-dao-postgres';
import {  AccountDAO } from "../src/daos/account-dao";
import { BankClient, Account, AccountType, ClientDetails } from '../src/entities';
import { AccountPostgres } from '../src/daos/account-dao-postgres';
import { BankClientDAO } from '../src/daos/bankClient-dao';


let testIsActive = true;
let testAmount = 1000;

const bankClientDAO: BankClientDAO = new BankClientPostgres();
const accountDAO: AccountDAO = new AccountPostgres();


test("-----TC 1: CREATE/POST Account for a Client-----", async()=>{
    let clientDetailsObject:ClientDetails = {
        firstName: 'TestAccount',
        lastName: 'N',
        ssn: 12413
    } 
    let clientObj:BankClient = new BankClient(0, clientDetailsObject);

    // persist client obj in database
    clientObj = await bankClientDAO.createClient(clientObj); 

    // get the client from database once created 
    const retrievedClient = await bankClientDAO.getClientById(clientObj.clientId);

    let testAccountType: AccountType = {
        account_type: "saving"
    }

    // create new account object
    const accountObj: Account = new Account(0, testAmount, testAccountType, testIsActive, retrievedClient.clientId);
    // persist account in database
    const createdAccount:Account = await accountDAO.createAccountForClient(accountObj);

    expect(createdAccount.accountId).not.toBe(0);
    
});


test("-----TC 2: READ/GET all Accounts for a Client-----", async()=>{
    let clientDetailsObject:ClientDetails = {
        firstName: 'Alex',
        lastName: 'Savedra',
        ssn: 45013
    } 
    let clientObj:BankClient = new BankClient(0, clientDetailsObject);
    const createdClient:BankClient =await bankClientDAO.createClient(clientObj);

    const retrievedClient = await bankClientDAO.getClientById(createdClient.clientId);

    let testAccountType1: AccountType = {
        account_type: "checking"
    };
    let testAccountType2: AccountType = {
        account_type: "saving"
    }

    const testAmount1 = 1035325;
    const testAmount2 = 40052;
    //create new account object
    const accountObj: Account = new Account(0, testAmount1, testAccountType1, testIsActive, retrievedClient.clientId);
    const accountObj2: Account = new Account(0, testAmount2, testAccountType2, testIsActive, retrievedClient.clientId);

    // persist account in database
    await accountDAO.createAccountForClient(accountObj);
    await accountDAO.createAccountForClient(accountObj2);


    const allAcounts:Account[] = await accountDAO.getAccountsForClient(retrievedClient.clientId);
    expect(allAcounts.length).toBeGreaterThanOrEqual(2);
});


test("-----TC 3: READ/GET Account by accountId-----", async()=> {
    console.log("------Attempting to GET ACCOUNT BY ID------");
    let clientDetailsObject:ClientDetails = {
        firstName: 'Jordan',
        lastName: 'P',
        ssn: 50313
    } 
    let clientObj:BankClient = new BankClient(0, clientDetailsObject);
    const createdClient:BankClient =await bankClientDAO.createClient(clientObj);

    const retrievedClient = await bankClientDAO.getClientById(createdClient.clientId);

    let testAccountType1: AccountType = {
        account_type: "checking"
    };
    let testAccountType2: AccountType = {
        account_type: "saving"
    }

    const testAmount1 = 15325;
    
    //create new account object
    const accountObj: Account = new Account(0, testAmount1, testAccountType1, testIsActive, retrievedClient.clientId);

    const accountCreated:Account = await accountDAO.createAccountForClient(accountObj);
    const account:Account = await accountDAO.getAccountById(accountCreated.accountId);
    console.log("----------Ending GET ACCOUNT BY ID------------------");
    expect(account.accountId).toBe(accountObj.accountId);
});

test("-----TC 4: DELETE Account by accountId-----", async()=> {
    console.log("------Attempting to DELETE ACCOUNT BY ID------");
    let clientDetailsObject:ClientDetails = {
        firstName: 'Phoebe',
        lastName: 'Cats',
        ssn: 69413
    } 
    let clientObj:BankClient = new BankClient(0, clientDetailsObject);
    const createdClient:BankClient = await bankClientDAO.createClient(clientObj);

    const retrievedClient = await bankClientDAO.getClientById(createdClient.clientId);

    let testAccountType1: AccountType = {
        account_type: "checking"
    };
    let testAccountType2: AccountType = {
        account_type: "saving"
    }

    const testAmount1 = 96925;
    
    //create new account object
    const accountObj: Account = new Account(0, testAmount1, testAccountType1, testIsActive, retrievedClient.clientId);

    const accountCreated:Account = await accountDAO.createAccountForClient(accountObj);
    const account:Boolean = await accountDAO.deleteAccountById(accountCreated.accountId);
    console.log("------Ending  DELETE ACCOUNT BY ID------");
    expect(account).toBeTruthy();
});

test("-----TC 5: Update Account by accountId-----", async() => {
    console.log("------Attempting to Update ACCOUNT BY ID------");
    let retrievedAccount = await accountDAO.getAccountById(1);
    retrievedAccount.accType.account_type = retrievedAccount.accType.account_type === 'checking'? 'saving':  'checking';
    retrievedAccount.amount = retrievedAccount.amount - 10;
    retrievedAccount.isActive = retrievedAccount.isActive === true ? false: true;
    retrievedAccount.clientId = retrievedAccount.clientId;
    
    const account:Account = await accountDAO.updateAccount(retrievedAccount);

    const verifiedClientObj = await accountDAO.getAccountById(account.accountId);

    console.log("------Ending UPDATE ACCOUNT BY ID------")
    expect(verifiedClientObj.accountId).toEqual(verifiedClientObj.accountId);

});

test("-----TC 6: DEPOSIT amount to account-----", async () => {
    console.log("Attempting to deposit amount to AccountId 1");
    const retrievedAccount = await accountDAO.getAccountById(1);
    const depositAmount:number  = 150;
    const result:Boolean = await accountDAO.depositAmount(retrievedAccount, depositAmount);
    console.log("------Ending  DEPOSIT ACCOUNT BY ID------");
    expect(result).toBeTruthy();

});

test("-----TC7 : WITHDRAW amount from Account-----", async()=>{
    console.log("Attempting to withdraw amount from AccountId 1");
    const retrievedAccount = await accountDAO.getAccountById(1);
    const withdrawAmount = 100;
    const result:Boolean = await accountDAO.withdrawAmount(retrievedAccount, withdrawAmount);
    console.log("------Ending  WITHDRAW ACCOUNT BY ID------");
    expect(result).toBeTruthy();
})


afterAll(async()=>{
    client.end()// should close our connection once all tests is over
})
