import {client} from '../src/connection';


test("Should create a connection ", async() => {
    const result = await client.query('select * from bankclient'); // we need await because we are 
    //fetching data stored on a database in the cloud
    // console.log(result);
});

afterAll(async()=>{
    client.end();// should close our connection once all tests is over
});